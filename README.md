# DnD Tokens
3D Models of various tokens for DnD Games designed to be 3D printed.

I try to design simple models so they can be printed without supports (if possible).

This repository contains (or will contain) following types of tokens:
 * Walls
 * Columns
 * Pillars
 * Decorations
 * Spells

I am not skilled enough to model characters or monsters, but I might try (probably not).

Models are created using FreeCAD and Blender and are completely open-source. You are welcome to use or edit them at will.
